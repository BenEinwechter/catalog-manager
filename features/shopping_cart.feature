Feature: Shopping Cart

  Background:
    Given I am logged in as a user

  Scenario: Add product to cart
    Given I am on the products page
    When I add a product to my cart
    Then the product is added to the cart
    When I view my cart
    Then I see a summary of my order

  Scenario: Remove product from cart
    Given I have some products in my cart
    When I view my cart
    Then I can remove products from my cart

  Scenario: Increment/decrement quantity of product in cart
    Given I have some products in my cart
    When I view my cart
    Then I can increment and decrement order quantities for products
    And when I decrement a product's quantity to zero
    Then the product is removed from my cart
