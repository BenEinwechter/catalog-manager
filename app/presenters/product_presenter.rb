class ProductPresenter
  def initialize(selected_category_id: nil)
    @selected_category_id = selected_category_id
  end

  def selected_category
    category = Category.where(id: @selected_category_id).take
    if category.nil?
      categories.first
    else
      category
    end
  end

  def categories
    @categories ||= Category.where(parent_id: nil).includes(:children).all
  end

  def products
    Product.where(category_id: selected_category_tree_ids).order(:name)
  end

  private

  def selected_category_tree_ids
    if selected_category
      [selected_category.id] + selected_category.children.ids
    else
      []
    end
  end
end
