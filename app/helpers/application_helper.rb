module ApplicationHelper
  def cart_total
    if current_user && current_user.cart
      current_user.cart.total.to_money
    else
      0.to_money
    end
  end

  def in_cart?(product)
    current_user.cart ? current_user.cart.contains?(product) : false
  end
end
