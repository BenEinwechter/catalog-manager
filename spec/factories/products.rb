FactoryGirl.define do
  factory :product do
    part_number '301201'
    name 'Log Cabin All Natural Pancake Mix'
    date 'UB 03/31/15'
    legend 'DS'
    unit '12/8 oz.'
    unit_multiplier 12
    unit_label 'ea.'
    quantity 162
    cost_cents 800
  end
end
