Rails.application.routes.draw do
  devise_for :users

  namespace :admin do
    resources :user_groups, except: [:destroy] do
      resources :group_orders, except: [:destroy]
    end
    root 'dashboard#show'
  end

  resources :products, only: [:index, :show] do
    collection do
      get 'category/:category_id', to: 'products#index', as: :category
    end
  end

  resource :cart, only: [:show] do
    resources :cart_items, only: [:create, :destroy], as: 'items', path: 'items' do
      member do
        patch :increment
        patch :decrement
      end
    end
    member do
      post :submit
    end
  end

  resources :orders, only: [:show]

  root 'products#index'
end
