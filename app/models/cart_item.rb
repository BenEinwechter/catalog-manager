class CartItem < ActiveRecord::Base
  belongs_to :cart
  belongs_to :product

  def description
    product.name
  end

  def price
    product.cost
  end

  def total
    quantity * price
  end
end
