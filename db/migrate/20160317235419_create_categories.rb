class CreateCategories < ActiveRecord::Migration
  def change
    create_table :categories do |t|
      t.integer :parent_id
      t.string  :name
      t.integer :source_id
      t.string  :source

      t.timestamps null: false
    end
  end
end
