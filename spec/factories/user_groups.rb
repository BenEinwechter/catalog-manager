FactoryGirl.define do
  factory :user_group do
    sequence(:name) { |n| "User Group #{n}" }
    address_1 '123 Some Rd'
    address_2 ''
    city 'Nashville'
    state 'TN'
    zip '37115'

    trait :with_active_order do
      after(:create) do |user_group|
        group_order = create(:group_order, user_group: user_group)
        user_group.update(active_group_order: group_order)
      end
    end
  end
end
