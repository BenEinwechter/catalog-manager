class UserGroupPolicy < ApplicationPolicy
  [:show?, :new?, :create?, :edit?, :update?].each do |name|
    define_method(name) { user.admin? }
  end

  class Scope < ApplicationPolicy::Scope
    def resolve
      if user.admin?
        scope.all
      else
        []
      end
    end
  end
end
