require 'rails_helper'

RSpec.describe Cart, type: :model do
  let(:cart) { Cart.create }
  let(:product) { create(:product) }

  describe '#add' do
    context 'product not in cart' do
      it 'adds a product and sets quantity to 1' do
        cart.add(product)
        expect(cart.items.count).to eq(1)
        item = cart.items.first
        expect(item.product).to eq(product)
        expect(item.quantity).to eq(1)
      end
    end

    context 'product already in cart' do
      it 'increments the product\'s quantity' do
        cart.add(product)
        cart.add(product)
        expect(cart.items.count).to eq(1)
        expect(cart.items.first.quantity).to eq(2)
      end
    end
  end

  describe '#contains?' do
    it 'returns a boolean indicating whether the product is in cart' do
      expect(cart.contains?(product)).to be false
      cart.add(product)
      expect(cart.contains?(product)).to be true
    end
  end

  describe '#subtotal' do
    it 'sums the totals of cart\'s products' do
      expect(cart.subtotal).to be_a(Money)
      expect(cart.subtotal).to eq(0)
      cart.add(product)
      expect(cart.subtotal).to eq(product.price)
      cart.add(product)
      expect(cart.subtotal).to eq(product.price * 2)
    end
  end

  describe '#shipping' do
    it 'returns $0.00 for an empty cart' do
      expect(cart.shipping).to be_a(Money)
      expect(cart.shipping).to eq 0.00
    end
  end

  describe '#tax' do
    it 'returns $0.00 for an empty cart' do
      expect(cart.tax).to be_a(Money)
      expect(cart.tax).to eq 0.00
    end
  end
end
