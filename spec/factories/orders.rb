FactoryGirl.define do
  factory :order do
    association :group_order
    association :user
  end
end
