FactoryGirl.define do
  factory :user do
    first_name 'John'
    last_name 'Doe'
    phone '123-234-3444'
    address_1 '123 Some Rd'
    city 'Holly Springs'
    state 'NC'
    zip '27540'
    sequence(:email) { |n| "person#{n}@example.com" }
    password '12345678'
    password_confirmation '12345678'

    trait :admin do
      admin true
    end

    trait :with_group do
      association :user_group, :with_active_order
    end
  end
end
