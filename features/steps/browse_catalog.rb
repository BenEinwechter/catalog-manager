class Spinach::Features::BrowseCatalog < Spinach::FeatureSteps
  before do
    @category = create(:category)
    @product = create(:product, category: @category)
  end

  step 'I am logged in as a user' do
    @user = create(:user)
    sign_in(@user)
  end

  step 'I visit the home page' do
    visit root_path
  end

  step 'I see a list of products' do
    expect(find('.products')).to have_content @product.name
  end
end
