FactoryGirl.define do
  factory :group_order do
    association :user_group
    scheduled_delivery { 1.week.from_now }
    deadline { 2.days.from_now }
    comments 'Some info about pickup'
    address_1 '123 Somewhere Rd'
    address_2 'Dock B'
    city 'Holly Springs'
    state 'NC'
    zip '27540'

    trait :with_orders do
      after(:create) do |group_order|
        create_pair(:order, group_order: group_order)
      end
    end
  end
end
