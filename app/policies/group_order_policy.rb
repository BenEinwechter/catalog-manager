class GroupOrderPolicy < ApplicationPolicy
  [:show?, :new?, :create?, :edit?, :update?].each do |name|
    define_method(name) { user.admin? }
  end
end
