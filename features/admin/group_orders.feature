Feature: Group Orders

  Background:
    Given I am logged in as an admin

  Scenario: Create a group order for a user group
    Given I am on the user groups page
    When I choose to create a new group order for a user group
    And I enter a delivery date, deadline, delivery address (defaults to user group's address)
    And save the form
    Then a group order is created and made the active order for that group

  Scenario: View group orders
    When I open a user group that has orders
    Then I see a list of the group's orders

  Scenario: View a group order
    When I open a group order
    Then I see all the user orders associated with the group order
    And I see the summary information for the group order
