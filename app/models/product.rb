class Product < ActiveRecord::Base
  belongs_to :category

  monetize :cost_cents

  def price
    cost #TODO add markup
  end
end
