class Admin::UserGroupsController < Admin::ApplicationController
  def index
    @user_groups = policy_scope(UserGroup)
  end

  def show
    @user_group = UserGroup.find(params[:id])
    authorize(@user_group)
  end

  def new
    @user_group = UserGroup.new
    authorize(@user_group)
  end

  def create
    @user_group = UserGroup.new(user_group_params)
    authorize(@user_group)
    if @user_group.save
      redirect_to admin_user_groups_url, notice: 'User group created successfully'
    else
      render :new
    end
  end

  def edit
    @user_group = UserGroup.find(params[:id])
    authorize(@user_group)
  end

  def update
    @user_group = UserGroup.find(params[:id])
    authorize(@user_group)
    if @user_group.update_attributes(user_group_params)
      redirect_to admin_user_groups_url, notice: 'User group updated successfully'
    else
      render :edit
    end
  end

  private

  def user_group_params
    params.require(:user_group).permit(:name, :address_1, :address_2, :city, :state, :zip)
  end
end
