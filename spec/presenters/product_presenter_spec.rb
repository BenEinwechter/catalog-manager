require 'rails_helper'

RSpec.describe ProductPresenter, type: :model do
  let!(:bakery) { Category.create!(name: 'Bakery') }
  let!(:bagels) { Category.create!(name: 'Bagels', parent: bakery) }
  let!(:bread)  { Category.create!(name: 'Bread',  parent: bakery) }
  let!(:cereal) { Category.create!(name: 'Cereal') }

  describe '#selected_category' do
    context 'when no selected_category_id is given' do
      let(:presenter) { ProductPresenter.new }

      it 'returns the first root category' do
        expect(presenter.selected_category).to eq bakery
      end
    end

    context 'when selected_category_id is a valid category ID' do
      let(:presenter) { ProductPresenter.new(selected_category_id: bread) }

      it 'returns the category for that ID' do
        expect(presenter.selected_category).to eq bread
      end
    end
  end

  describe '#products' do
    let!(:generic_bakery_product) { create(:product, name: 'Generic Bakery', category: bakery) }
    let!(:bagel_product) { create(:product, name: 'Blueberry Bagels', category: bagels) }
    let!(:bread_product) { create(:product, name: 'White Bread', category: bread) }
    let!(:cereal_product) { create(:product, name: 'Cornflakes', category: cereal) }

    let(:presenter) { ProductPresenter.new(selected_category_id: bakery) }

    it 'returns products directly within the selected category and its children' do
      expect(presenter.products.pluck(:name)).to eq(
        [
          'Blueberry Bagels',
          'Generic Bakery',
          'White Bread'
        ]
      )
    end
  end
end
