class Admin::DashboardController < Admin::ApplicationController
  def show
    authorize(:dashboard, :show?)
  end
end
