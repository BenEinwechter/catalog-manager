class GroupOrder < ActiveRecord::Base
  belongs_to :user_group
  has_many :orders

  def total
    return 0.to_money if orders.empty?
    orders.map(&:total).reduce(:+)
  end
end
