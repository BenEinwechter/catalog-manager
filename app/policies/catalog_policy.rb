class CatalogPolicy < ApplicationPolicy
  def show?
    user.present?
  end
end
