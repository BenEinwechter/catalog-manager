class AddFieldsToProducts < ActiveRecord::Migration
  def change
    add_column :products, :layer, :string
    rename_column :products, :unit_count, :unit_multiplier
    change_column :products, :unit_multiplier, :decimal
  end
end
