class User < ActiveRecord::Base
  has_one :cart, dependent: :destroy
  belongs_to :user_group
  has_many :orders

  devise :database_authenticatable, :registerable, :recoverable,
         :rememberable, :trackable, :validatable

  def full_name
    [first_name, last_name].join(' ').strip
  end
end
