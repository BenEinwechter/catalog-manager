FactoryGirl.define do
  factory :cart do
    association :user

    trait :with_items do
      after(:create) do |cart|
        create(:cart_item, cart: cart)
      end
    end
  end

  factory :cart_item do
    association :cart
    association :product
    quantity 1
  end
end
