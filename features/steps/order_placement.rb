class Spinach::Features::OrderPlacement < Spinach::FeatureSteps
  step 'I am logged in as a user' do
    @user = create(:user, :with_group)
    sign_in(@user)
  end

  step 'I have items in my cart' do
    @cart = create(:cart, user: @user)
    @product = create(:product, cost: 30.00)
    @cart.add(@product)
  end

  step 'I submit my order' do
    view_cart
    click_on 'Place Order'
  end

  step 'I see a confirmation page' do
    expect(page).to have_content('Your order has been successfully received.')
  end

  step 'a order is created from my cart\'s contents' do
    @order = Order.last
    expect(page).to have_content("Order ##{@order.id}")
    expect(page).to have_content("Status: pending")
    expect(find('table#line_items')).to have_content(@product.name)
  end

  step 'the order is associated with my user group\'s active order' do
    expect(page).to have_content("Group Order ID: ##{@user.user_group.active_group_order.id}")
  end
end
