ENV['RAILS_ENV'] = 'test'

require './config/environment'
require 'database_cleaner'
require 'rspec/expectations'

DatabaseCleaner.strategy = :truncation

Spinach.hooks.before_scenario { DatabaseCleaner.clean }
