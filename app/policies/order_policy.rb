class OrderPolicy < ApplicationPolicy
  def show?
    user.admin? || record.user == user
  end
end
