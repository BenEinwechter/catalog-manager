Feature: Order Placement

  Scenario: Submit order
    Given I am logged in as a user
    And I have items in my cart
    When I submit my order
    Then I see a confirmation page
    And a order is created from my cart's contents
    And the order is associated with my user group's active order
