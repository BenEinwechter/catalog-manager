Feature: User Groups

  Background:
    Given I am logged in as an admin

  Scenario: View all user groups
    Given some user groups exist
    When I visit the user groups page
    Then I'm shown a list of all user groups

  Scenario: Create user group
    When I choose to create a new group from the user groups page
    And enter a group name and default delivery address and save the form
    Then the user group is created

  Scenario: Edit user group
    Given a user group exists
    When I choose to edit that group from the user groups page
    And change the group's name and delivery address save the form
    Then the user group is updated
