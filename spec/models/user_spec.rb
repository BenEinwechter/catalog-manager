require 'rails_helper'

RSpec.describe User, type: :model do
  describe '#full_name' do
    it 'combines the first and last names' do
      expect(User.new.full_name).to eq ''
      expect(User.new(first_name: 'Ben').full_name).to eq 'Ben'
      expect(User.new(last_name: 'Smith').full_name).to eq 'Smith'
      expect(User.new(first_name: 'Ben', last_name: 'Smith').full_name).to eq 'Ben Smith'
    end
  end
end
