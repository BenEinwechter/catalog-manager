class CartsController < ApplicationController
  def show
    authorize(cart)
    @cart = cart
  end

  def submit
    authorize(cart)

    if current_group_order.nil?
      redirect_to cart_url, alert: 'No active group order!' and return
    end

    order = OrderProcessor.convert_cart_to_order!(cart, current_group_order)
    redirect_to order_url(order), notice: 'Your order has been successfully received. Thank you!'
  end

  private

  def current_group_order
    current_user.try(:user_group).try(:active_group_order)
  end
end
