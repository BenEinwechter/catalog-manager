class LineItem < ActiveRecord::Base
  belongs_to :order
  belongs_to :product

  monetize :price_cents
  monetize :total_cents
end
