module DSL
  module Navigation
    def cart_summary
      find('.header__cart')
    end

    def open_user_groups_page
      click_on 'Admin'
      click_on 'User Groups'
    end

    def view_cart
      visit(cart_path)
    end
  end
end
