class RemoveDescriptionFromCartItem < ActiveRecord::Migration
  def change
    remove_column :cart_items, :description, :string
  end
end
