class OrderProcessor
  def self.convert_cart_to_order!(cart, group_order)
    raise 'cart contains no items' if cart.items.empty?
    order = Order.new(user: cart.user, group_order: group_order)
    cart.items.each do |cart_item|
      order.line_items.build(
        product: cart_item.product,
        description: cart_item.description,
        quantity: cart_item.quantity,
        price: cart_item.price,
        total: cart_item.price * cart_item.quantity
      )
    end
    order.subtotal = order.line_items.inject(0) { |sum, li| sum + li.total }
    order.total = order.subtotal + order.shipping + order.tax
    order.save!
    cart.destroy!
    order
  end
end
