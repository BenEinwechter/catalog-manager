class Spinach::Features::GroupOrders < Spinach::FeatureSteps
  include DSL::UserGroups

  before do
    @group = create(:user_group)
  end

  step 'I am logged in as an admin' do
    @admin = create(:user, :admin)
    sign_in(@admin)
  end

  step 'I am on the user groups page' do
    open_user_groups_page
  end

  step 'I choose to create a new group order for a user group' do
    row_for_user_group(@group).click_on 'New Group Order'
  end

  step 'I enter a delivery date, deadline, delivery address (defaults to user group\'s address)' do
    @deliver_date = DateTime.parse('2020-01-31T10:00:00-06:00')
    @deadline = DateTime.parse('2020-01-21T23:59:00-06:00')
    select_datetime('scheduled_delivery', @deliver_date)
    select_datetime('deadline', @deadline)

    expect(find_field('group_order[address_1]').value).to eq @group.address_1
    expect(find_field('group_order[address_2]').value).to eq @group.address_2
    expect(find_field('group_order[city]').value).to eq @group.city
    expect(find_field('group_order[state]').value).to eq @group.state
    expect(find_field('group_order[zip]').value).to eq @group.zip

    fill_in 'Comments', with: 'A comment about the order'
  end

  step 'save the form' do
    click_on 'Create Group order'
  end

  step 'a group order is created and made the active order for that group' do
    expect(page).to have_content 'Group order created successfully!'
    @group.reload
    expect(@group.group_orders.count).to eq 1
    group_order = @group.group_orders.last
    expect(@group.active_group_order).to eq group_order
    expect(group_order.address_1).to eq @group.address_1
    expect(group_order.address_2).to eq @group.address_2
    expect(group_order.city).to eq @group.city
    expect(group_order.state).to eq @group.state
    expect(group_order.zip).to eq @group.zip
    expect(group_order.comments).to eq 'A comment about the order'
  end

  step 'I open a user group that has orders' do
    @group_orders = create_list(:group_order, 2, user_group: @group)
    open_user_group(@group)
  end

  step 'I see a list of the group\'s orders' do
    @group_orders.each do |order|
      expect(find('table#group_orders')).to have_selector("tr[data-group-order-id='#{order.id}']")
    end
  end

  step 'I open a group order' do
    @group_order = create(:group_order, :with_orders, user_group: @group)
    open_user_group(@group)
    find('table#group_orders').click_on(@group_order.id)
    expect(page).to have_content("Group Order ##{@group_order.id}")
  end

  step 'I see all the user orders associated with the group order' do
    @group_order.orders.each do |order|
      order_container = find("[data-order-id='#{order.id}']")
      expect(order_container).to have_content("Order #{order.id} - #{order.user.full_name}")
    end
  end

  step 'I see the summary information for the group order' do
    summary = find('#summary')
    expect(summary).to have_content("Order count: #{@group_order.orders.count}")
    expect(summary).to have_content("Total: #{@group_order.total.format}")
  end

  private

  def open_user_group(group)
    open_user_groups_page
    click_on @group.name
  end

  def select_datetime(field_name, datetime)
    select datetime.strftime('%Y'), from: "group_order[#{field_name}(1i)]"
    select datetime.strftime('%B'), from: "group_order[#{field_name}(2i)]"
    select datetime.strftime('%e'), from: "group_order[#{field_name}(3i)]"
    select datetime.strftime('%H'), from: "group_order[#{field_name}(4i)]"
    select datetime.strftime('%M'), from: "group_order[#{field_name}(5i)]"
  end
end
