class CartItemPolicy < ApplicationPolicy
  [:destroy?, :increment?, :decrement?].each do |name|
    define_method(name) { record.cart.user == user }
  end
end
