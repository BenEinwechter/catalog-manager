require 'csv'

module Importers
  class DWF
    attr_reader :io

    def initialize(io)
      @io = io
    end

    def process!
      csv = CSV.new(io, { headers: true })
      csv.each() do |row|
        row = format_row(row)
        product_category = create_categories(row)

        product = Product.find_or_initialize_by(part_number: row[:id])
        product.update!(
          category: product_category,
          legend: row[:legend],
          quantity: row[:available],
          name: row[:description],
          date: row[:date],
          layer: row[:layer],
          unit: row[:unit],
          unit_label: row[:unit_label],
          unit_multiplier: row[:case_units],
          cost: row[:price]
        )
      end
    end

    private

    def create_categories(row)
      if row[:parent_category_id] == 1
        # DWF makes all their root categories children of a special category named 'INVENTORY'
        # with an id of '1'. Our system doesn't need that root category.
        parent = nil
      else
        parent = Category.find_or_initialize_by(source_id: row[:parent_category_id])
        parent.update!(
          name: row[:parent_category_description],
          source_id: row[:parent_category_id],
          source: 'DWF'
        )
      end

      category = Category.find_or_initialize_by(source_id: row[:category_id])
      category.update!(
        parent: parent,
        name: row[:category_description],
        source_id: row[:category_id],
        source: 'DWF'
      )
      category
    end

    def format_row(row)
      {
        legend: row['Lgn'].to_s.strip,
        id: row['ID'].to_s.strip,
        available: row['Available'].to_i,
        description: row['Description'].to_s.strip,
        date: row['Date'].to_s.strip,
        layer: row['Layer/Skid'].to_s.strip,
        unit: row['Unit'].to_s.strip,
        unit_label: row['Unit Label'].to_s.strip,
        case_units: row['Case Units'].to_d,
        price: row['Price'].to_d,
        category_description: row['Cat Desc'].to_s.strip.titlecase,
        category_id: row['Cat ID'].to_i,
        parent_category_description: row['Parent Cat Desc'].to_s.strip.titlecase,
        parent_category_id: row['Parent Cat ID'].to_i
      }
    end
  end
end
