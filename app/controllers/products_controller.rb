class ProductsController < ApplicationController
  def index
    skip_policy_scope
    authorize(:catalog, :show?)
    @presenter = ProductPresenter.new(selected_category_id: params[:category_id])
  end

  def show
  end
end
