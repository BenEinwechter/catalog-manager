class CreateCartItems < ActiveRecord::Migration
  def change
    create_table :cart_items do |t|
      t.integer :cart_id
      t.integer :product_id
      t.string :description
      t.integer :quantity, default: 0, null: false

      t.timestamps null: false

      t.index :cart_id
      t.index :product_id
    end
  end
end
