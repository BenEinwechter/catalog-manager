class Spinach::Features::UserGroups < Spinach::FeatureSteps
  include DSL::UserGroups
  
  step 'I am logged in as an admin' do
    @admin = create(:user, :admin)
    sign_in(@admin)
  end

  step 'some user groups exist' do
    @group1, @group2 = create_pair(:user_group)
  end

  step 'a user group exists' do
    @group = create(:user_group)
  end

  step 'I visit the user groups page' do
    open_user_groups_page
  end

  step 'I\'m shown a list of all user groups' do
    expect(page).to have_content @group1.name
    expect(page).to have_content @group2.name
  end

  step 'I choose to create a new group from the user groups page' do
    open_user_groups_page
    click_on 'New User Group'
  end

  step 'enter a group name and default delivery address and save the form' do
    @create_values = {
      name: 'A test group',
      address_1: '123 Long Rd',
      address_2: 'Parking Lot',
      city: 'Holly Springs',
      state: 'NC',
      zip: '27540',
    }
    fill_in_form(@create_values)
    click_on 'Create User group'
  end

  step 'the user group is created' do
    expect(page).to have_content('User group created successfully')
    expect(UserGroup.count).to eq 1
    verify(model: UserGroup.last, has_expect_values: @create_values)
  end

  step 'I choose to edit that group from the user groups page' do
    open_user_groups_page
    row_for_user_group(@group).click_on 'Edit'
  end

  step 'change the group\'s name and delivery address save the form' do
    @update_values = {
      name: 'An updated name',
      address_1: '123 New Rd',
      address_2: 'Parking Lot B',
      city: 'Stevens',
      state: 'PA',
      zip: '17578'
    }
    fill_in_form(@update_values)
    click_on 'Update User group'
  end

  step 'the user group is updated' do
    expect(page).to have_content('User group updated successfully')
    verify(model: @group.reload, has_expect_values: @update_values)
  end

  private

  def fill_in_form(values)
    fill_in 'Name',      with: values[:name]
    fill_in 'Address 1', with: values[:address_1]
    fill_in 'Address 2', with: values[:address_2]
    fill_in 'City',      with: values[:city]
    fill_in 'State',     with: values[:state]
    fill_in 'Zip',       with: values[:zip]
  end

  def verify(model:, has_expect_values:)
    has_expect_values.each do |field, value|
      expect(model.send(field)).to eq value
    end
  end
end
