class Order < ActiveRecord::Base
  belongs_to :user
  belongs_to :group_order
  has_many :line_items, dependent: :destroy

  enum status: [:pending]

  monetize :subtotal_cents, :shipping_cents, :tax_cents, :total_cents

  def group_name
    group_order.user_group.name
  end
end
