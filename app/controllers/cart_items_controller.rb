class CartItemsController < ApplicationController
  def create
    authorize(cart, :show?)
    product = Product.find(params[:product_id])
    cart.add(product)
    redirect_to products_url
  end

  def destroy
    item = CartItem.find(params[:id])
    authorize(item)
    item.destroy!
    redirect_to cart_url
  end

  def increment
    item = CartItem.find(params[:id])
    authorize(item)
    item.increment!(:quantity)
    redirect_to cart_url
  end

  def decrement
    item = CartItem.find(params[:id])
    authorize(item)
    if item.quantity > 1
      item.decrement!(:quantity)
    else
      item.destroy!
    end
    redirect_to cart_url
  end
end
