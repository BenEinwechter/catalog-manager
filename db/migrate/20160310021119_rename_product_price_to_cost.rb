class RenameProductPriceToCost < ActiveRecord::Migration
  def change
    rename_column :products, :price_cents, :cost_cents
  end
end
