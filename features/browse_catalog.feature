Feature: Browse Catalog

  Scenario: View list of products
    Given I am logged in as a user
    When I visit the home page
    Then I see a list of products
