class Admin::GroupOrdersController < Admin::ApplicationController
  def new
    @user_group = UserGroup.find(params[:user_group_id])
    @group_order = @user_group.group_orders.new
    authorize(@group_order)
    @group_order.address_1 = @user_group.address_1
    @group_order.address_2 = @user_group.address_2
    @group_order.city = @user_group.city
    @group_order.state = @user_group.state
    @group_order.zip = @user_group.zip
  end

  def create
    @user_group = UserGroup.find(params[:user_group_id])
    @group_order = @user_group.group_orders.new(group_order_params)
    authorize(@group_order)
    if @group_order.save
      @user_group.update!(active_group_order: @group_order)
      redirect_to admin_user_group_group_order_url(@user_group, @group_order), notice: 'Group order created successfully!'
    else
      render :new
    end
  end

  def show
    @user_group = UserGroup.find(params[:user_group_id])
    @group_order = @user_group.group_orders.find(params[:id])
    authorize(@group_order)
  end

  private

  def group_order_params
    permitted = [:scheduled_delivery, :deadline, :address_1, :address_2, :city, :state, :zip, :comments]
    params.require(:group_order).permit(*permitted)
  end
end
