class CartPolicy < ApplicationPolicy
  def show?
    record.user == user
  end

  def submit?
    record.user == user
  end
end
