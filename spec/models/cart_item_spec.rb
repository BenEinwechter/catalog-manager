require 'rails_helper'

RSpec.describe CartItem do
  let(:product) { build(:product, cost: 2.00) }

  describe '#total' do
    it 'returns quantity * product price' do
      item = CartItem.new(product: product, quantity: 1)
      expect(item.total).to eq 2.00
      item.quantity = 2
      expect(item.total).to eq 4.00
    end
  end
end
