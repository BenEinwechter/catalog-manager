require 'rails_helper'

RSpec.describe GroupOrder, type: :model do
  describe '#total' do
    context 'no orders' do
      let(:group_order) { GroupOrder.new }

      it 'returns zero dollars' do
        expect(group_order.total).to be_a_kind_of(Money)
        expect(group_order.total).to eq 0.00
      end
    end

    context 'with associated orders' do
      let(:group_order) do
        group_order = create(:group_order)
        create(:order, group_order: group_order, total: 50.00 )
        create(:order, group_order: group_order, total: 25.25 )
        group_order
      end

      it 'returns the total dollars for all orders' do
        expect(group_order.total).to be_a_kind_of(Money)
        expect(group_order.total).to eq 75.25
      end
    end
  end
end
