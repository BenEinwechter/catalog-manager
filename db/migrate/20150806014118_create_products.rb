class CreateProducts < ActiveRecord::Migration
  def change
    create_table :products do |t|
      t.string :part_number
      t.string :name
      t.string :date
      t.string :legend
      t.string :unit
      t.integer :unit_count
      t.string :unit_label
      t.integer :quantity
      t.integer :price_cents
      t.timestamps null: false
    end
  end
end
