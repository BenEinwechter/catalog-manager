class Cart < ActiveRecord::Base
  belongs_to :user
  has_many :items, -> { order(:id) }, class_name: 'CartItem', dependent: :destroy

  def add(product)
    item = find_item_for(product)

    if item
      item.increment(:quantity)
    else
      item = items.build(product: product, quantity: 1)
    end

    item.save!
  end

  def contains?(product)
    find_item_for(product).present?
  end

  def subtotal
    items.inject(0.to_money) { |total, item| total += item.total }
  end

  def shipping
    0.to_money
  end

  def tax
    0.to_money
  end

  def total
    subtotal + shipping + tax
  end

  private

  def find_item_for(product)
    items.detect { |item| item.product_id == product.id }
  end
end
