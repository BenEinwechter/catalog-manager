class UserGroup < ActiveRecord::Base
  has_many :users
  has_many :group_orders
  belongs_to :active_group_order, class_name: 'GroupOrder'

  validates :name, presence: true, uniqueness: true
end
