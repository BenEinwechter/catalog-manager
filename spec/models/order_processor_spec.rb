require 'rails_helper'

RSpec.describe OrderProcessor, type: :model do
  describe '.convert_cart_to_order!' do
    let(:group_order) { create(:group_order) }
    let(:cart) { create(:cart) }

    context 'when cart has no items' do
      it 'raises an exception' do
        expect {
          OrderProcessor.convert_cart_to_order!(cart, group_order)
        }.to raise_error(RuntimeError, 'cart contains no items')
      end
    end

    context 'when cart has items' do
      let(:product) { create(:product, cost: 9.0) }
      before do
        cart.items.create(product: product, quantity: 2)
      end

      it 'creates a PENDING order associated with the given group order' do
        order = OrderProcessor.convert_cart_to_order!(cart, group_order)
        expect(order.pending?).to be true
        expect(group_order.orders.count).to eq 1
        expect(group_order.orders.last).to eq order
      end

      it 'creates an order associated with the cart\'s user' do
        order = OrderProcessor.convert_cart_to_order!(cart, group_order)
        expect(cart.user.orders.count).to eq 1
        expect(cart.user.orders.last).to eq order
      end

      it 'creates line items from cart items' do
        order = OrderProcessor.convert_cart_to_order!(cart, group_order)
        expect(order.line_items.count).to eq 1
        line_item = order.line_items.last
        expect(line_item.product).to eq product
        expect(line_item.description).to eq product.name
        expect(line_item.quantity).to eq 2
        expect(line_item.price).to eq 9.00
        expect(line_item.total).to eq 18.00
      end

      it 'calculates the order\'s totals' do
        order = OrderProcessor.convert_cart_to_order!(cart, group_order)
        expect(order.subtotal).to eq 18.00
        expect(order.total).to eq 18.00
      end

      it 'destroys the cart record' do
        expect(Cart.count).to eq 1
        expect(CartItem.count).to eq 1
        OrderProcessor.convert_cart_to_order!(cart, group_order)
        expect(Cart.count).to eq 0
        expect(CartItem.count).to eq 0
      end
    end
  end
end
