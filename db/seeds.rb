# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).

csv = File.new("#{Rails.root}/db/data/denver.csv")
Importers::DWF.new(csv).process!

tn_group = UserGroup.where(name: 'TN Group').first_or_create!(
  address_1: '123 Some Rd',
  city: 'Nashville',
  state: 'TN',
  zip: '37115'
)

UserGroup.where(name: 'NC Group').first_or_create!(
  address_1: '123 Long Rd',
  city: 'Holly Springs',
  state: 'NC',
  zip: '27540'
)

User.where(email: 'user@example.com').first_or_create!(
  first_name: 'John',
  last_name: 'Doe',
  password: '12345678',
  password_confirmation: '12345678',
  user_group: tn_group
)

User.where(email: 'admin@example.com').first_or_create!(
  first_name: 'Ben',
  last_name: 'Doe',
  password: '12345678',
  password_confirmation: '12345678',
  admin: true
)
