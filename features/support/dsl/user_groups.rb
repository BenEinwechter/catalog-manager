module DSL
  module UserGroups
    def row_for_user_group(user_group)
      find("tr[data-user-group-id='#{user_group.id}']")
    end
  end
end
