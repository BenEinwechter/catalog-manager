class Spinach::Features::ShoppingCart < Spinach::FeatureSteps
  before do
    category = create(:category)
    @product1 = create(:product, cost: 10.00, category: category)
    @product2 = create(:product, cost:  5.00, category: category)
  end

  step 'I am logged in as a user' do
    @user = create(:user)
    sign_in(@user)
  end

  step 'I am on the products page' do
    expect(page).to have_css("[data-product-id='#{@product1.id}']")
  end

  step 'I add a product to my cart' do
    add_product_to_cart(@product1)
  end

  step 'the product is added to the cart' do
    expect(cart_summary).to have_content @user.cart.total.format
    expect(page).to have_css("[class*='product--added'][data-product-id='#{@product1.id}']")
  end

  step 'I view my cart' do
    view_cart
  end

  step 'I see a summary of my order' do
    expect(page).to have_current_path(cart_path)
    row = cart_table_row(@product1)
    expect(row.find('.description')).to have_content(@product1.name)
    expect(row.find('.quantity')).to have_content('1')
    expect(row.find('.total')).to have_content(@product1.price.format)

    expect(order_summary).to have_content 'Order Summary'
    expect(order_summary.find('.subtotal')).to have_content "Items (#{@user.cart.items.count})"
    expect(order_summary.find('.shipping')).to have_content @user.cart.shipping.format
    expect(order_summary.find('.tax')).to have_content @user.cart.tax.format
    expect(order_summary.find('.total')).to have_content @user.cart.total.format
  end

  step 'I have some products in my cart' do
    add_product_to_cart(@product1)
    add_product_to_cart(@product2)
  end

  step 'I can remove products from my cart' do
    cart_table_row(@product1).click_on('x')
    expect(cart_table).to have_no_css("tr[data-product-id='#{@product1.id}']")
  end

  step 'I can increment and decrement order quantities for products' do
    expect(cart_table_row(@product1).find('.quantity')).to have_content('1')
    cart_table_row(@product1).click_on('+')
    expect(cart_table_row(@product1).find('.quantity')).to have_content('2')
    cart_table_row(@product1).click_on('-')
    expect(cart_table_row(@product1).find('.quantity')).to have_content('1')
  end

  step 'when I decrement a product\'s quantity to zero' do
    cart_table_row(@product1).click_on('-')
  end

  step 'the product is removed from my cart' do
    expect(cart_table).to have_no_css("tr[data-product-id='#{@product1.id}']")
  end

  private

  def cart_table
    find('table#cart_items')
  end

  def cart_table_row(product)
    cart_table.find("tr[data-product-id='#{product.id}']")
  end

  def order_summary
    find('#order_summary')
  end

  def add_product_to_cart(product)
    find("[data-product-id='#{product.id}']").click_on 'Add'
  end
end
