class AddActiveOrderToUserGroups < ActiveRecord::Migration
  def change
    add_column :user_groups, :active_group_order_id, :integer
  end
end
