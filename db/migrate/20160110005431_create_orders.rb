class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.references :user
      t.references :group_order
      t.integer :status,         default: 0, null: false
      t.integer :subtotal_cents, default: 0, null: false
      t.integer :shipping_cents, default: 0, null: false
      t.integer :tax_cents,      default: 0, null: false
      t.integer :total_cents,    default: 0, null: false

      t.timestamps null: false
    end
  end
end
