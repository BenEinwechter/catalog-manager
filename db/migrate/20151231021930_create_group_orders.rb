class CreateGroupOrders < ActiveRecord::Migration
  def change
    create_table :group_orders do |t|
      t.references :user_group, index: true, foreign_key: true
      t.datetime :scheduled_delivery
      t.datetime :deadline
      t.text :comments
      t.string :address_1
      t.string :address_2
      t.string :city
      t.string :state
      t.string :zip

      t.timestamps null: false
    end
  end
end
