require 'rails_helper'

RSpec.describe Importers::DWF, type: :model do
  describe '#process!' do
    let(:io) do
      io = StringIO.new
      io.puts(%q["Lgn","ID","Available","Description","Date","Layer/Skid","Unit","Unit Label","Case Units","Price","Parent Cat ID","Parent Cat Desc","Cat ID","Cat Desc"])
      io.puts(%q["F","0000700",2,"Lender's 2 oz. Cinnamon Swirl Bagels (6 pk.)","BB 03/09/16","9 / 72","12/12 oz.","ea.",12.00,7.80,"  133","BAKERY","   96","Bagels"])
      io.puts(%q["D","338751",19,"Gatorade Fruit Punch Thirst Quencher","02/13/16","10 / 70","12/32 oz.","ea.",12.00,8.40,"  134","BEVERAGE","   99","Soda"])
      io.puts(%q["D","285941",42,"Food Club Tasty O's Cereal","UB 05/28/16","6 / 48","12/14 oz.","ea.",12.00,16.80,"    1","INVENTORY","   73","CEREAL"])
      io.rewind
      io
    end

    let(:importer) {  Importers::DWF.new(io) }

    context 'no existing products' do
      before(:each) { importer.process! }

      it 'creates new categories' do
        expect(Category.count).to eq 5

        parent = Category.find_by(source_id: 133)
        expect(parent.name).to eq 'Bakery'
        expect(parent.parent_id).to be nil
        expect(parent.source).to eq 'DWF'

        child = Category.find_by(source_id: 96)
        expect(child.name).to eq 'Bagels'
        expect(child.parent).to eq parent
        expect(child.source).to eq 'DWF'

        root_with_no_children = Category.find_by!(source_id: 73)
        expect(root_with_no_children.name).to eq 'Cereal'
        expect(root_with_no_children.children.count).to eq 0
      end

      it 'creates new products' do
        expect(Product.count).to eq 3
        product = Product.find_by(part_number: '0000700')
        expect(product.legend).to eq 'F'
        expect(product.quantity).to eq 2
        expect(product.name).to eq %q[Lender's 2 oz. Cinnamon Swirl Bagels (6 pk.)]
        expect(product.date).to eq 'BB 03/09/16'
        expect(product.layer).to eq '9 / 72'
        expect(product.unit).to eq '12/12 oz.'
        expect(product.unit_label).to eq 'ea.'
        expect(product.unit_multiplier).to eq 12.0
        expect(product.cost).to eq 7.80
      end

      it 'associates the products with the correct category' do
        product = Product.find_by(part_number: '0000700')
        category = Category.find_by!(source_id: 96)
        expect(product.category).to eq category

        product = Product.find_by(part_number: '338751')
        category = Category.find_by!(source_id: 99)
        expect(product.category).to eq category
      end
    end

    context 'existing products' do
      it 'finds existing products by part number and overwrites their values' do
        create(:product, part_number: '0000700')
        importer.process!
        expect(Product.count).to eq 3
        product = Product.find_by!(part_number: '0000700')
        expect(product.legend).to eq 'F'
        expect(product.quantity).to eq 2
        expect(product.name).to eq %q[Lender's 2 oz. Cinnamon Swirl Bagels (6 pk.)]
        expect(product.date).to eq 'BB 03/09/16'
        expect(product.layer).to eq '9 / 72'
        expect(product.unit).to eq '12/12 oz.'
        expect(product.unit_label).to eq 'ea.'
        expect(product.unit_multiplier).to eq 12.0
        expect(product.cost).to eq 7.80
      end

      it 'updates existing categories' do
        bakery = Category.create(name: 'Existing Bakery', source_id: 133)
        Category.create(name: 'Existing Bagels Sub', source_id: 96, parent: bakery)
        Category.create(name: 'Soda', source_id: 99, parent: bakery)

        importer.process!

        expect(Category.count).to eq 5

        bakery = Category.find_by!(source_id: 133)
        expect(bakery.name).to eq 'Bakery'
        expect(bakery.children.pluck(:name)).to eq ['Bagels']

        beverage = Category.find_by!(source_id: 134)
        expect(beverage.name).to eq 'Beverage'
        expect(beverage.children.pluck(:name)).to eq ['Soda']
      end
    end
  end
end
