class CreateLineItems < ActiveRecord::Migration
  def change
    create_table :line_items do |t|
      t.references :order, index: true, foreign_key: true
      t.references :product, index: true, foreign_key: true
      t.string :description
      t.decimal :quantity
      t.integer :price_cents
      t.integer :total_cents

      t.timestamps null: false
    end
  end
end
